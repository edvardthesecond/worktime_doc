#!/usr/bin/env python2
# -*- coding: utf-8 -*-

### README ###
# Dieses Skript erstellt automatisch alle Arbeitszeitblätter
# für einen Hiwi in der Praktikumsbetreuung und Übungsgruppenbetreuung.
#
# 'berechnung_feiertage.py' ist für die Berücksichtigung der Feiertage nötig.
# Die Ausgabe wird mit pdflatex erstellt.
#
# Die Arbeitszeitregeln für Hilfskräfte werden beachtet.
###

import berechnung_feiertage as bf
from datetime import *
from random import randint
from subprocess import call
import os as os
from string import replace

####################
###  Variabeln
####################

# Bundesland
state = 'BW'
# maximal 10 Stunden am Tag
max_day_work = 10
# maximal 19.5 Stunden pro Woche
max_week_work = 19.5
# maximale Arbeitszeit am Stück
break_interval = 6
min_break_time = 0.5
max_break_time = 1.5
# Arbeitszeit zwischen 0630 und 2000
start_time = time(6,30)
end_time = time(20,00)
# Vertragsdaten
institut = 'Physikalisches Institut'

# noch zu ändernde Daten siehe get_input
name = 'ein Name'
startdate = datetime(2017, 9, 15)
enddate = datetime(2018, 2, 14)
hours_per_month = 20
# Weihnachtsferien
weihnachtsferien = True
# Arbeitszeit am Kurstag
course_days_times = []
course_days = []

real_work_time = 5

### globale Arbeits-Variabeln
worklist = []
shared_hours = 0


### Name und Vertragsdaten
def get_input():
    name = raw_input('Dein Name : ')
    while True:
        try:
            start = raw_input('Startdatum des Vertrags < dd-mm-yyyy > : ')
            startdate = datetime.strptime(start, "%d-%m-%Y")
            break
        except ValueError:
            print 'fehlerhaftes Format... {}'.format(start)
            
    while True:
        try:
            end = raw_input('Enddatum des Vertrags < dd-mm-yyyy > : ')
            enddate = datetime.strptime(end, "%d-%m-%Y")
            if enddate <= startdate:
                print 'Enddatum {} ist zu klein...'.format(end)
            else :
                break
        except ValueError:
            print 'fehlerhaftes Format... {}'.format(end)

    while True:
        try:
            hours = raw_input('Stunden pro Monat : ')
            hours_per_month = int(hours)
            if hours_per_month > 4*max_week_work:
                print "Du hast wohl keinen Minijob..."
            else:
                break
        except ValueError:
            print 'fehlerhaftes Format... {}'.format(hours)

    course_days = []
    course_days_times = []
    while True:
        try:
            day = int(raw_input('Kurstag (Mo = 0 , Di = 1 , ...) : '))
            if day > 6 or day < 0:
                print('Eingabe muss zwischen 0 und 4 liegen')
            elif day > 4:
                print('Wochenende??')
            elif day not in course_days:
                start_time = raw_input('Arbeitszeitbeginn am Kurstag < HH:MM > : ')
                course_start_time = datetime.strptime(start_time, "%H:%M").time()
                max_course_day_work = int(raw_input('maximale Arbeitszeit am Kurstag : '))
                course_days.append(day)
                course_days_times.append((day, course_start_time, max_course_day_work))
            y = raw_input('weiteren Kurstag hinzufügen? (y/n) : ')
            if y != 'y':
                break
        except ValueError:
            print('fehlerhaftes Format...')
    while True:
        try:
            real_work_time = raw_input('maximale zufällige Arbeitszeit pro Tag (min 2h): ' )
            real_work_time = int(real_work_time)
            if real_work_time < 2:
                print('wäre etwas wenig...')
            else:
                break
        except ValueError:
            print('fehlerhaftes Format...')

    return name, startdate, enddate, hours_per_month, course_days, course_days_times, real_work_time

### abgekürzter Wochentag
def print_day(day):
    if day == 0:
        return 'Mo'
    elif day == 1:
        return 'Di'
    elif day == 2:
        return 'Mi'
    elif day == 3:
        return 'Do'
    elif day == 4:
        return 'Fr'
    elif day == 5:
        return 'Sa'
    elif day == 6:
        return 'So'
    else:
        print("Error in Funktion print_day ...")
        quit()

### abgekürzter Monat
def print_month(i):
    if i == 1:
        return 'Januar'
    elif i == 2:
        return 'Februar'
    elif i == 3:
        return 'März'
    elif i == 4:
        return 'April'
    elif i == 5:
        return 'Mai'
    elif i == 6:
        return 'Juni'
    elif i == 7:
        return 'Juli'
    elif i == 8:
        return 'August'
    elif i == 9:
        return 'September'
    elif i == 10:
        return 'Oktober'
    elif i == 11:
        return 'November'
    elif i == 12:
        return 'Dezember'
    else:
        print('Error in Funktion get_month ...')
        quit()
    
### alle gesetzlichen Feiertage + 24.12. + 31.12.
def get_all_holidays():
    try:
        L = []
        for year in range(startdate.year, enddate.year+1):
            hl = bf.Holidays(year, state)
            hl_ = hl.get_holiday_list()
            for i in hl_:
                L.append(i[0])
            L.append(date(year, 12, 24))
            L.append(date(year, 12, 31))
            if weihnachtsferien:
                for (i, j) in [(12,27), (12,28), (12,29), (12,30), (01,01), (01,02), (01,03), (01,04), (01,05)]:
                    L.append(date(year, i, j))
        return sorted(L)
    except:
        print "Error in Funktion get_all_holidays ..."
        quit()

### Werktag-Abfrage
def workday(day, banned_days):
    if day.date() in banned_days:
        return False
    elif day.weekday() > 4:
        return False
    else:
        return True

### Anzahl der Monate im Vertragszeitraum
def n_months():
    N_day = int(float(enddate.day - startdate.day)/30)
    N_month = enddate.month - startdate.month
    N_year = ( enddate.year - startdate.year ) * 12
    #print(N_day, N_month, N_year)
    return int(N_day + N_month + N_year)

### Stunden diese Woche
def get_week_work(day, worklist):
    try:
        hours = 0
        week =  [ ((day + timedelta(i - day.weekday())).date()) for i in range(7) ]
        for workday in worklist:
            if workday.date() in week:
                hours += workday.hour
        return hours
    except:
        print "Error in Funktion get_week_work ..."
        quit()

### Verteilung der Stunden auf den Kurstag
def base_worktime(shared_hours, worklist, banned_days):
    for i in range(len(course_days_times)):
        course_day = course_days_times[i][0]
        max_course_day_work = course_days_times[i][2]
        weeks = 0
        first_day = startdate + timedelta((course_day - startdate.weekday())%7)
        d = first_day
        while d <= enddate:
            if workday(d, banned_days):
                weeks += 1
            d += timedelta(7)
        dayly_work = min(int(max_hours/(weeks*len(course_days))), max_course_day_work, real_work_time)
        d = first_day
        while d <= enddate and (shared_hours + dayly_work) <= max_hours:
            if workday(d, banned_days):
                shared_hours += dayly_work
                worklist.append(d + timedelta(hours=dayly_work))
                banned_days.append(d.date())
            d += timedelta(7)
    return shared_hours, worklist, banned_days

### zufällige Verteilung der restlichen Stunden auf alle übrigen Arbeitstage
def random_worktime(shared_hours, worklist, banned_days):
    while shared_hours < max_hours:
        diff = max_hours - shared_hours
        r_day = startdate + timedelta(1) * randint(0, n_days )
        restart = False
        while True:
            if workday(r_day, banned_days):
                week_hours = int(get_week_work(r_day, worklist))
                if week_hours < int(max_week_work):
                    r_time = min(randint(1, min(diff,real_work_time)), max_day_work, int(max_week_work - week_hours))
                    shared_hours += r_time
                    worklist.append(r_day + timedelta(hours=r_time))
                    banned_days.append(r_day.date())
                    break
            r_day += timedelta(randint(1, 6))
            if r_day > enddate:
                r_day = startdate
                if restart:
                    print "zu viele Arbeitsstunden..."
                    quit()
                restart = True
                print "starting again..."
    return shared_hours,  sorted(worklist), banned_days

### Erstellung der Uhrzeiten inklusive Pause
def work_time(d):
    w_hours = d.hour
    break_quaters = 0
    if break_interval < float(max_day_work/2):
        print("Ich rechne nur mit einer Pause...")
        quit()
    t_1 = ''
    t_2 = ''
    p_1 = ''
    p_2 = ''
    if w_hours != 0:
        if w_hours > break_interval:
            break_quaters = randint(4 * min_break_time, 4 * max_break_time)
        s_time = datetime.combine(d.date(), start_time)
        e_time = datetime.combine(d.date(), end_time)
        if d.weekday() in course_days:
            for i in range(len(course_days_times)):
                if course_days_times[i][0] == d.weekday():
                    t2 = datetime.combine(d.date(), course_days_times[i][1]) + timedelta(hours = 1) * min(w_hours, course_days_times[i][2])
                    t1 = t2 - timedelta(minutes = 15) * (break_quaters + w_hours * 4)
        else:
            day_hours = (e_time.hour - s_time.hour) + (e_time.minute - s_time.minute)/60
            t2 = s_time + timedelta(minutes = 15) * randint(w_hours * 4 + break_quaters, day_hours * 4)
            t1 = t2 - timedelta(minutes = 15) * (4 * w_hours + break_quaters)
        if break_quaters != 0:
            p1 = t1 + timedelta(minutes = 15) * randint(4 * min(break_interval, max_day_work - break_interval), 4 * max(break_interval, max_day_work - break_interval))
            p2 = p1 + timedelta(minutes = 15) * break_quaters
            p_1 = '{:02d}:{:02d}'.format(p1.hour, p1.minute)
            p_2 = '{:02d}:{:02d}'.format(p2.hour, p2.minute)
        t_1 = '{:02d}:{:02d}'.format(t1.hour, t1.minute)
        t_2 = '{:02d}:{:02d}'.format(t2.hour, t2.minute)
    return t_1, t_2, p_1, p_2

### Letzter Tag des Monats
def end_of_month(mydate):
    d = datetime(mydate.year + ( mydate.month / 12), ((mydate.month % 12) + 1), 1) - timedelta(days = 1)
    return d 

### Liste aller Tage im Vertragszeitraum
def day_list(worklist):
    d = startdate.replace(day=1)
    day_list = []
    i_max = len(worklist)
    i = 0
    while d <= end_of_month(enddate):
        if i < i_max and d.date() == worklist[i].date():
            day_list.append(worklist[i])
            i += 1
        else:
            day_list.append(d)
        d += timedelta(1)
    return day_list

# Inhalt der tex-Tabelle
def table_content(day_list, m, y):
    tex_content = ''
    this_month = []
    this_month_work = 0
    for i in day_list:
        if i.month == m:
            if i.year == y:
                this_month.append(i)
    for d in this_month:
        t1, t2, p1, p2 = work_time(d)
        tex_content += '{0}, {1} & {2} & {3} & {4} & {5} & & '.format(print_day(d.weekday()), d.day,
        t1, p1, p2, t2)
        if d.hour == 0:
            tex_content += '\\tabularnewline \hline\n'
        else:
            tex_content += '{}\,h \\tabularnewline \hline\n'.format(d.hour)
        if d.weekday() == 6 and not d == this_month[-1]:
            tex_content += '\multicolumn{7}{c}{} \\tabularnewline \hline\n'
        this_month_work += d.hour
    return tex_content, this_month_work

# Pdf-Output
def create_pdf(day_list, m, y, shared_hours):
    header = """\documentclass[11pt,a4paper,DIV=19]{scrartcl}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{helvet}
\\renewcommand{\\familydefault}{\sfdefault}
\usepackage{tabularx}
\\newlength{\mywidth}
\setlength{\mywidth}{0.09\\textwidth}
\\newcolumntype{C}{>{\centering} X <{}}
\\newcolumntype{P}{>{\centering} p{\mywidth} <{}}
\\begin{document}
\\thispagestyle{empty}"""
    top_table = """\section*{Arbeitszeitblatt im %s %i}
    \\begin{tabularx}{\linewidth}{lC}
        Institut: & %s \\tabularnewline
        Name: & %s \\tabularnewline
        monatliche Arbeitszeit: & %i\,h \n\\end{tabularx} \n""" % (print_month(m), y, institut, name, hours_per_month)
    mid_table = """\\begin{center}
  \\begin{tabularx}{\linewidth}{|P|P|P|P|P|C|C|}\hline
    \\textbf{Datum} & \\textbf{Beginn} & \multicolumn{2}{c|}{\\textbf{Mittagspause}} & \\textbf{Ende} & \\textbf{Bemerkungen} & \\textbf{Arbeitszeit} \\tabularnewline \hline \hline """
    content, hours_sum = table_content(day_list, m, y)
    shared_hours += hours_sum
    mid_table += content
    mid_table += """\multicolumn{7}{c}{}\\tabularnewline 
    \multicolumn{6}{r}{Summe:} & \multicolumn{1}{c}{%i\,h} \\tabularnewline \cline{5-7}
  \\end{tabularx}""" % (hours_sum)
    signature = """\\vfill
\\end{center} 
\\begin{tabular}{p{0.5\linewidth}}
  \dotfill \\tabularnewline
  \centering  Datum, Unterschrift des Arbeitnehmers
\\end{tabular}
\\end{document}"""

    latex_out = header + top_table + mid_table + signature
    outfilename = 'Arbeitszeitblatt_{}_{}-{:02d}.pdf'.format(replace(name, ' ', '-'), y, m)
    tempname = 'temp_{}-{:02d}'.format(y, m)

    if os.path.isfile(outfilename):
        y = raw_input('%s existiert. überschreiben? (y/n) : ' % (outfilename))
        if y != 'y':
            print("Wohin soll die Ausgabe denn sonst? So kann ich nicht arbeiten!")
            quit()

    tempfilenames = [tempname + i for i in ('.aux', '.log', '.pdf', '.tex')]
    for f in tempfilenames:
        if os.path.isfile(f):
            y = raw_input('%s existiert. Brauchst du die Datei noch? (y/n) : ' % f)
            if y == 'y':
                print("So kann ich nicht arbeiten!")
                quit()
            
    f = open(tempname + '.tex', 'w')
    f.write(latex_out)
    f.close()
    devnull = open(os.devnull, 'w')
    latex = call(['pdflatex', tempname + '.tex'], stdout=devnull)
    os.rename(tempname + '.pdf', outfilename)
    for i in [tempname + i for i in ('.aux', '.log', '.tex')]:
        os.remove(i)
    print("%s erstellt ..." % (outfilename))
    return shared_hours

# Ausgabe aller wichtigen Daten
def print_all():
    print "\n\t### Daten ###"
    print "Bundesland \t\t{}".format(state)
    print "Arbeitszeit zwischen \t{}:{:02d} & {}:{:02d}".format(start_time.hour, start_time.minute, end_time.hour, end_time.minute)
    print "maximale Arbeitszeit \t{}h pro Tag \n\t\t\t{}h pro Woche\n\t\t\t{}h am Stück".format(max_day_work, max_week_work, break_interval)
    print "Pausenlänge (zufällig) \t{}h bis {}h".format(min_break_time, max_break_time)
    print("\nVertragsdaten: \t\t{} \n\t\t\t{} \n\t\t\t{} bis {} \n\t\t\t{}h monatlich".format(institut, name, startdate.date(), enddate.date(), hours_per_month))
    for i in course_days_times:
        print(" \nKurstag \t\t{}".format(print_day(i[0])))
        print("Kursstart\t\t{}:{:02d}".format(i[1].hour, i[1].minute))
        print("max Kursarbeitszeit\t{}h".format(i[2]))
    print("\nmaximale Arbeitszeit\t{}h".format(real_work_time))
    print("Gesamtstunden \t\t{}h".format(max_hours))
    print("Vertragsmonate \t\t{}".format(n_months))
    print "Gesamttage \t\t{}".format(n_days)
#    print "verbotene Tage:"
#    for i in banned_days:
#        print "\t\t\t{}".format(i)
    print("Erstellte Arbeitstage: \n")
    print("Tag \t\tStunden\tvon\tbis\tPause")
    for i in worklist:
        t1, t2, p1, p2 = work_time(i)
        print("{}, {}\t  {}\t{}\t{}\t{}\t{}".format(print_day(i.weekday()), i.date(), i.hour, t1, t2, p1, p2))
        

name, startdate, enddate, hours_per_month, course_days, course_days_times, real_work_time = get_input()
n_months = n_months()
max_hours = n_months * hours_per_month
n_days = (enddate - startdate).days + 1

banned_days = get_all_holidays()
shared_hours, worklist, banned_days = base_worktime(shared_hours, worklist, banned_days)
shared_hours, worklist, banned_days = random_worktime(shared_hours, worklist, banned_days)

print_all()

y = raw_input('\nSind die Daten oben korrekt?\npdf ausgeben? (y/n) : ')
if y == 'y':
    d = startdate
    shared_hours = 0
    day_list = day_list(worklist)
    while d <= enddate:
        shared_hours = create_pdf(day_list, d.month, d.year, shared_hours)
        m = d.month
        while d.month == m:
            d += timedelta(1)
